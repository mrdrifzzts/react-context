import React from 'react'
import store from '../store/store'
import { Provider } from 'react-redux'
import { providingDebaton } from '../context/context'

class Home extends React.Component {
  render () {
    const { Component, pageProps } = this.props
    return (
      <div>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </div>
    )
  }
}

export default providingDebaton(Home)
