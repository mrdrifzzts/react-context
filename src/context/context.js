import React from 'react'

const DebatonContext = React.createContext()

class SuperComplicatedClass {
  constructor(complicatedId) {
    this.complicatedId = complicatedId;
  }

  async createComplicatedObject(){
    return {
      string: 'peperoni',
      number: 4,
      multiply: (x,y) => x*y,
      shouldLogIn: () => true,
      shouldLogInButComplicated: async () => true
    }
  }

  async setUser(name){
    this.name = name
  }
}

const SuperComplicatedInstance = new SuperComplicatedClass(1)


export function consumingDebaton (Component) {
  return class extends Component {
    render () {
      return (
        <DebatonContext.Consumer>
          {context => <Component {...this.props} context={context} />}
        </DebatonContext.Consumer>
      )
    }
  }
}

export function providingDebaton (Component) {
  return class extends Component {
    constructor (props) {
      super(props)
      this.state = {
        complicatedObject: null
      }
    }

    async forceStateUpdate (state) {
      await new Promise(resolve => this.setState(state, resolve))
    }

    async loadComplicatedObject () {
      let { complicatedObject } = this.state
      if (complicatedObject === null) {
        complicatedObject = await SuperComplicatedInstance.createComplicatedObject()
        await this.forceStateUpdate({ complicatedObject })
      }
    }

    async logIn (name) {
      await this.loadComplicatedObject()
      let { complicatedObject } = this.state
      let shouldLogIn = await complicatedObject.shouldLogInButComplicated()
      if (shouldLogIn) {
        await complicatedObject.setUser(name)
      }
    }

    render () {
      return (
        <DebatonContext.Provider
          value={{
            ...this.state,
            loadComplicatedObject: this.loadComplicatedObject.bind(this),
            logIn: this.logIn.bind(this)
          }}
        >
          <Component {...this.props} />
        </DebatonContext.Provider>
      )
    }
  }
}
