export const SAVE_USER = 'mb/auth/SAVE_USER'

export const saveUser = user => dispatch => dispatch({ type: SAVE_USER, user })
