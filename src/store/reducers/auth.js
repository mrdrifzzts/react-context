import { SAVE_USER } from '../actions/auth'

const initialState = {
  user: {
    name: ''
  },
  loggedIn: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_USER:
      return {
        ...state,
        user: action.payload,
        loggedIn: true
      }
    default:
      return state
  }
}
