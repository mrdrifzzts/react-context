import React, { Component } from 'react';
import { connect } from 'react-redux'
import { logIn, logOut } from './store/actions/auth'
import StoreContainer from './containers/StoreContainer'
import './App.css';

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  logIn: user => dispatch(logIn(user)),
  logOut: () => logOut(dispatch)
})

class StoreApp extends Component {
  render() {
    return <StoreContainer {...this.props} />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreApp);
