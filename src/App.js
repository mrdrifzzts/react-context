import React, { Component } from 'react';
import { connect } from 'react-redux'
import './App.css';
import { providingDebaton } from './context/context'
import Home from './containers/Home'
import { saveUser } from './store/actions/auth'

const mapStateToProps = state => ({
  auth: state.auth,
})

const mapDispatchToProps = dispatch => {
  return {
    saveUser: () => saveUser(dispatch)
  }
}

class App extends Component {
  render() {
    return <Home {...this.props} />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(providingDebaton(App));
